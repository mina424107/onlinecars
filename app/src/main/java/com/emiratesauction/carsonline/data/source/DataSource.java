package com.emiratesauction.carsonline.data.source;


import com.emiratesauction.carsonline.data.source.remote.models.CarsResponse;

import rx.Observable;

/**
 * Created by mina farid on 08/07/2018.
 */

public interface DataSource {

    Observable<CarsResponse> getCarsOnline();


}

