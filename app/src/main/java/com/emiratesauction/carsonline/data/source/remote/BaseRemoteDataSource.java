package com.emiratesauction.carsonline.data.source.remote;

  import com.emiratesauction.carsonline.BuildConfig;
  import com.emiratesauction.carsonline.app.Constants;

  import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

/**
 * Created by mina farid on 08/07/2018.
 */

public class BaseRemoteDataSource {
    private static Retrofit retrofit;
    private static OkHttpClient okHttpClient;


    public <T> T createService(Class<T> service) {
        return getRetrofit().create(service);
    }

    public Retrofit getRetrofit() {
        if (retrofit == null) {
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getOkHttpClient());
            return builder.build();
        }
        return retrofit;
    }

    public static OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                builder.addInterceptor(logging);

            }
            okHttpClient = builder.readTimeout(5, TimeUnit.MINUTES).connectTimeout(5, TimeUnit.MINUTES).build();
        }
        return okHttpClient;
    }

}
