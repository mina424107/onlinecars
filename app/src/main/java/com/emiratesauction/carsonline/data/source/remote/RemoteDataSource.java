package com.emiratesauction.carsonline.data.source.remote;


import com.emiratesauction.carsonline.data.source.DataSource;
import com.emiratesauction.carsonline.data.source.remote.models.CarsResponse;

import rx.Observable;

/**
 * Created by mina farid on 08/07/2018.
 */

public class RemoteDataSource extends BaseRemoteDataSource implements DataSource {

    @Override
    public Observable<CarsResponse> getCarsOnline() {
        return createService(DataApi.class).getCarsOnline();
    }


}