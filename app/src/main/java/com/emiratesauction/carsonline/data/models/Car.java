package com.emiratesauction.carsonline.data.models;

import com.emiratesauction.carsonline.utils.Utils;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mina farid on 09/07/2018.
 */
public class Car {


    @SerializedName("carID")
    private int carID;

    @SerializedName("image")
    private String image;

    @SerializedName("descriptionAr")
    private String descriptionAr;

    @SerializedName("descriptionEn")
    private String descriptionEn;

    @SerializedName("imgCount")
    private int imgCount;

    @SerializedName("sharingLink")
    private String sharingLink;

    @SerializedName("sharingMsgEn")
    private String sharingMsgEn;

    @SerializedName("sharingMsgAr")
    private String sharingMsgAr;

    @SerializedName("mileage")
    private String mileage;

    @SerializedName("makeID")
    private int makeID;

    @SerializedName("modelID")
    private int modelID;

    @SerializedName("bodyId")
    private int bodyId;

    @SerializedName("year")
    private int year;

    @SerializedName("makeEn")
    private String makeEn;

    @SerializedName("makeAr")
    private String makeAr;

    @SerializedName("modelEn")
    private String modelEn;

    @SerializedName("modelAr")
    private String modelAr;

    @SerializedName("bodyEn")
    private String bodyEn;

    @SerializedName("bodyAr")
    private String bodyAr;

    @SerializedName("AuctionInfo")
    private AuctionInfo AuctionInfo;


    /*
    setters and getters
     */
    public int getCarID() {
        return carID;
    }

    public void setCarID(int carID) {
        this.carID = carID;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescriptionAr() {
        return descriptionAr;
    }

    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public int getImgCount() {
        return imgCount;
    }

    public void setImgCount(int imgCount) {
        this.imgCount = imgCount;
    }

    public String getSharingLink() {
        return sharingLink;
    }

    public void setSharingLink(String sharingLink) {
        this.sharingLink = sharingLink;
    }

    public String getSharingMsgEn() {
        return sharingMsgEn;
    }

    public void setSharingMsgEn(String sharingMsgEn) {
        this.sharingMsgEn = sharingMsgEn;
    }

    public String getSharingMsgAr() {
        return sharingMsgAr;
    }

    public void setSharingMsgAr(String sharingMsgAr) {
        this.sharingMsgAr = sharingMsgAr;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public int getMakeID() {
        return makeID;
    }

    public void setMakeID(int makeID) {
        this.makeID = makeID;
    }

    public int getModelID() {
        return modelID;
    }

    public void setModelID(int modelID) {
        this.modelID = modelID;
    }

    public int getBodyId() {
        return bodyId;
    }

    public void setBodyId(int bodyId) {
        this.bodyId = bodyId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getMakeEn() {
        return makeEn;
    }

    public void setMakeEn(String makeEn) {
        this.makeEn = makeEn;
    }

    public String getMakeAr() {
        return makeAr;
    }

    public void setMakeAr(String makeAr) {
        this.makeAr = makeAr;
    }

    public String getModelEn() {
        return modelEn;
    }

    public void setModelEn(String modelEn) {
        this.modelEn = modelEn;
    }

    public String getModelAr() {
        return modelAr;
    }

    public void setModelAr(String modelAr) {
        this.modelAr = modelAr;
    }

    public String getBodyEn() {
        return bodyEn;
    }

    public void setBodyEn(String bodyEn) {
        this.bodyEn = bodyEn;
    }

    public String getBodyAr() {
        return bodyAr;
    }

    public void setBodyAr(String bodyAr) {
        this.bodyAr = bodyAr;
    }

    public com.emiratesauction.carsonline.data.models.AuctionInfo getAuctionInfo() {
        return AuctionInfo;
    }

    public void setAuctionInfo(com.emiratesauction.carsonline.data.models.AuctionInfo auctionInfo) {
        AuctionInfo = auctionInfo;
    }

   public String getCarName()
    {
        if(Utils.isDeviceEnglish())
           return getMakeEn() + " " + getModelEn() + " " + getYear();
        else
            return getMakeAr() + " " + getModelAr() + " " + getYear();

    }

}