package com.emiratesauction.carsonline.data.source.remote;


import com.emiratesauction.carsonline.data.source.remote.models.CarsResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by mina farid on 08/07/2018.
 */

public interface DataApi {
     @GET("carsonline")
     Observable<CarsResponse> getCarsOnline();

}
