package com.emiratesauction.carsonline.data.source;

import android.content.Context;

import com.emiratesauction.carsonline.data.source.remote.Remote;
import com.emiratesauction.carsonline.data.source.remote.models.CarsResponse;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;


/**
 * Created by mina farid on 08/07/2018.
 */
@Singleton
public class DataRepository implements DataSource {


    private DataSource mRemoteDataSource;
    private Context mContext;

    @Inject
    public DataRepository(@Remote DataSource dataSource, Context context) {
        this.mRemoteDataSource = dataSource;
        this.mContext = context;
    }


    @Override
    public Observable<CarsResponse> getCarsOnline() {
        return mRemoteDataSource.getCarsOnline();
    }

}
