package com.emiratesauction.carsonline.data.source.remote.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mina farid on 08/07/2018.
 */

public class BaseResponse {


    @SerializedName("alertEn")
    private String alertEn;
    @SerializedName("alertAr")
    private String alertAr;
    @SerializedName("Error")
    private com.emiratesauction.carsonline.data.models.Error Error;


    /*
 setters and getters
  */
    public String getAlertEn() {
        return alertEn;
    }

    public void setAlertEn(String alertEn) {
        this.alertEn = alertEn;
    }

    public String getAlertAr() {
        return alertAr;
    }

    public void setAlertAr(String alertAr) {
        this.alertAr = alertAr;
    }

    public com.emiratesauction.carsonline.data.models.Error getError() {
        return Error;
    }

    public void setError(com.emiratesauction.carsonline.data.models.Error error) {
        Error = error;
    }

}