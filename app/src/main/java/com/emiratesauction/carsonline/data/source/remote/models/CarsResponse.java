package com.emiratesauction.carsonline.data.source.remote.models;

import com.emiratesauction.carsonline.data.models.Car;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mina farid on 08/07/2018.
 */

public class CarsResponse extends BaseResponse {


    @SerializedName("RefreshInterval")
    private int RefreshInterval;
    @SerializedName("Ticks")
    private String Ticks;
    @SerializedName("count")
    private int count;
    @SerializedName("endDate")
    private int endDate;
    @SerializedName("sortOption")
    private String sortOption;
    @SerializedName("sortDirection")
    private String sortDirection;
    @SerializedName("Cars")
    private List<Car> Cars;


    /*
    setters and getters
     */

    public int getRefreshInterval() {
        return RefreshInterval;
    }

    public void setRefreshInterval(int refreshInterval) {
        RefreshInterval = refreshInterval;
    }

    public String getTicks() {
        return Ticks;
    }

    public void setTicks(String ticks) {
        Ticks = ticks;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getEndDate() {
        return endDate;
    }

    public void setEndDate(int endDate) {
        this.endDate = endDate;
    }

    public String getSortOption() {
        return sortOption;
    }

    public void setSortOption(String sortOption) {
        this.sortOption = sortOption;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public List<Car> getCars() {
        return Cars;
    }

    public void setCars(List<Car> cars) {
        Cars = cars;
    }


}