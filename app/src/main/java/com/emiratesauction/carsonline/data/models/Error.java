package com.emiratesauction.carsonline.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mina farid on 09/07/2018.
 */
public class Error {

    @SerializedName("code")
    private int code;

    @SerializedName("en")
    private String en;

    @SerializedName("ar")
    private String ar;


    /*
    setters and getters
     */
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public String getAr() {
        return ar;
    }

    public void setAr(String ar) {
        this.ar = ar;
    }


}
