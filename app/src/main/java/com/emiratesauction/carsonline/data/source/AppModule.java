package com.emiratesauction.carsonline.data.source;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mina farid on 08/07/2018.
 */
@Module
public class AppModule {
    private final Context mContext;

    public AppModule(Context mApplication) {
        this.mContext = mApplication;
    }

    @Provides
    @Singleton
    Context provideApplication() {
        return mContext;
    }
}
