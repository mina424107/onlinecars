package com.emiratesauction.carsonline.data.source;


import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by mina farid on 08/07/2018.
 */
@Singleton
@Component(modules = {AppModule.class, DataRepositoryModule.class})
public interface DataRepositoryComponent {
        DataRepository getDataRepository();
}
