package com.emiratesauction.carsonline.data.models;

import com.emiratesauction.carsonline.utils.Utils;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mina farid on 09/07/2018.
 */
public class AuctionInfo {

    @SerializedName("bids")
    private int bids;
    @SerializedName("endDate")
    private int endDate;
    @SerializedName("endDateEn")
    private String endDateEn;
    @SerializedName("endDateAr")
    private String endDateAr;
    @SerializedName("currencyEn")
    private String currencyEn;
    @SerializedName("currencyAr")
    private String currencyAr;
    @SerializedName("currentPrice")
    private Long currentPrice;
    @SerializedName("minIncrement")
    private int minIncrement;
    @SerializedName("lot")
    private int lot;
    @SerializedName("priority")
    private int priority;
    @SerializedName("VATPercent")
    private int VATPercent;
    @SerializedName("isModified")
    private int isModified;
    @SerializedName("itemid")
    private int itemid;
    @SerializedName("iCarId")
    private int iCarId;
    @SerializedName("iVinNumber")
    private String iVinNumber;


    /*
    setters and getters
     */
    public int getBids() {
        return bids;
    }

    public void setBids(int bids) {
        this.bids = bids;
    }

    public int getEndDate() {
        return endDate;
    }

    public void setEndDate(int endDate) {
        this.endDate = endDate;
    }

    public String getEndDateEn() {
        return endDateEn;
    }

    public void setEndDateEn(String endDateEn) {
        this.endDateEn = endDateEn;
    }

    public String getEndDateAr() {
        return endDateAr;
    }

    public void setEndDateAr(String endDateAr) {
        this.endDateAr = endDateAr;
    }

    public String getCurrencyEn() {
        return currencyEn;
    }

    public void setCurrencyEn(String currencyEn) {
        this.currencyEn = currencyEn;
    }

    public String getCurrencyAr() {
        return currencyAr;
    }

    public void setCurrencyAr(String currencyAr) {
        this.currencyAr = currencyAr;
    }

    public Long getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(Long currentPrice) {
        this.currentPrice = currentPrice;
    }

    public int getMinIncrement() {
        return minIncrement;
    }

    public void setMinIncrement(int minIncrement) {
        this.minIncrement = minIncrement;
    }

    public int getLot() {
        return lot;
    }

    public void setLot(int lot) {
        this.lot = lot;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getVATPercent() {
        return VATPercent;
    }

    public void setVATPercent(int VATPercent) {
        this.VATPercent = VATPercent;
    }

    public int getIsModified() {
        return isModified;
    }

    public void setIsModified(int isModified) {
        this.isModified = isModified;
    }

    public int getItemid() {
        return itemid;
    }

    public void setItemid(int itemid) {
        this.itemid = itemid;
    }

    public int getiCarId() {
        return iCarId;
    }

    public void setiCarId(int iCarId) {
        this.iCarId = iCarId;
    }

    public String getiVinNumber() {
        return iVinNumber;
    }

    public void setiVinNumber(String iVinNumber) {
        this.iVinNumber = iVinNumber;
    }

    public String getCurrency()
    {
        if(Utils.isDeviceEnglish())
            return getCurrencyEn();
        else
            return getCurrencyAr();

    }
}
