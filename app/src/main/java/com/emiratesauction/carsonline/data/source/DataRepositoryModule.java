package com.emiratesauction.carsonline.data.source;

import com.emiratesauction.carsonline.data.source.remote.Remote;
import com.emiratesauction.carsonline.data.source.remote.RemoteDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mina farid on 08/07/2018.
 */
@Module
public class DataRepositoryModule {
    @Singleton
    @Provides
    @Remote
    DataSource provideDataRepository() {
        return new RemoteDataSource();
    }


}
