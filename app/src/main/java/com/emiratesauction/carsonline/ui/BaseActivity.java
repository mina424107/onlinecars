package com.emiratesauction.carsonline.ui;

import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.emiratesauction.carsonline.R;
import com.emiratesauction.carsonline.utils.Utils;
import com.emiratesauction.carsonline.utils.receivers.NetworkStatusReceiver;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * Created by mina farid on 09/07/2018.
 */
public class BaseActivity extends AppCompatActivity implements NetworkStatusReceiver.NetworkStatusListener {
    NetworkStatusReceiver networkStatusReceiver;

    private Snackbar snackbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onResume() {
        super.onResume();
        AddConnectivityListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        RemoveConnectivityListener();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    /*
     * super function when all listeners are ok
     */
    @CallSuper
    public void WhenAllIsGood() {

    }

    /*
     *check function
     */

    public void checkingNetworkAvailablity() {
        if (Utils.isOnline(this)) {
            WhenAllIsGood();
            HideReloadBar();
        } else {
            if (!Utils.isOnline(this)) {
                ShowReloadBar(getString(R.string.no_internet_message));
            }

        }
    }

    /*
     * Network
     */
    void AddConnectivityListener() {
        networkStatusReceiver = new NetworkStatusReceiver();
        registerReceiver(networkStatusReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    void RemoveConnectivityListener() {
        unregisterReceiver(networkStatusReceiver);
    }

    @Override
    public void onNetworkFail() {
        checkingNetworkAvailablity();
    }

    @Override
    public void onNetworkConnected() {
        checkingNetworkAvailablity();
    }

    /*
     *Snackbar

     */

    public void ShowReloadBar(String msg) {
        snackbar = Snackbar
                .make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                checkingNetworkAvailablity();
                            }
                        }, 1000);
                    }
                });

        // Changing message text color
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorAccent));

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        // change snack bar background
        sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        snackbar.show();
    }

    public void HideReloadBar() {
        if (snackbar != null)
            snackbar.dismiss();
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}