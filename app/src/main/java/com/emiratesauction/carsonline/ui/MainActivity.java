package com.emiratesauction.carsonline.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

import com.emiratesauction.carsonline.R;
import com.emiratesauction.carsonline.adapter.CarsAdapter;
import com.emiratesauction.carsonline.app.App;
import com.emiratesauction.carsonline.app.Constants;
import com.emiratesauction.carsonline.data.models.Car;
import com.emiratesauction.carsonline.data.source.remote.models.CarsResponse;
import com.emiratesauction.carsonline.di.CarsPresenterModule;
import com.emiratesauction.carsonline.di.DaggerCarsComponent;
import com.emiratesauction.carsonline.di.presenter.CarsContract;
import com.emiratesauction.carsonline.di.presenter.CarsPresenterImpl;
import com.emiratesauction.carsonline.utils.EndlessRecyclerViewScrollListener;
import com.emiratesauction.carsonline.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class MainActivity extends BaseActivity implements CarsContract.View {


    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;

    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    ImageView imageView;
    @Inject
    CarsPresenterImpl mPresenter;

    ArrayList<Car> mCars, mTotalCars;
    CarsAdapter mCarsAdapter;
    EndlessRecyclerViewScrollListener mEndlessRecyclerViewScrollListener;
    Subscription mSubscription;
    private boolean isLoading, isUpdating, isFirstTimeToRefresh = true;
    private int start, end;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        DaggerCarsComponent.builder().
                dataRepositoryComponent(((App) getApplication()).getNetComponent()).
                carsPresenterModule(new CarsPresenterModule(this))
                .build().
                inject(this);

        initialiseUI();

//        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
//         toolbar.inflateMenu(R.menu.menu);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    void refreshCars(int refreshInterval) {
        mSubscription = Observable.interval(refreshInterval, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> {
                    isUpdating = true;
                    mSwipeRefreshLayout.setRefreshing(true);
                    checkingNetworkAvailablity(); // check network and get data
                });

    }

    void updateRecyclerView(List<Car> mUpdatedTotalCars) {

        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }

        mTotalCars.clear();
        mTotalCars.addAll(mUpdatedTotalCars);
        int finalSize = mCars.size();
        mCars.clear();
        mCars.addAll(new ArrayList<>(mTotalCars.subList(0, finalSize)));
        mCarsAdapter.notifyDataSetChanged();
    }

    private void initialiseUI() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        ((DefaultItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        mRecyclerView.setHasFixedSize(true);
        mCars = new ArrayList<>();
        mTotalCars = new ArrayList<>();

        // scroll listener
        mEndlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                isLoading = true;
                if (mCars.size() < mTotalCars.size()) // check to disable pagination
                {
                    mCars.add(null);
                    mCarsAdapter.notifyItemInserted(mCars.size() - 1);


                    // delay 1 sec to simulate network request
                    Observable.timer(1000, TimeUnit.MILLISECONDS)
                            .map(o -> showCarsList(page))
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe();

                }
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        };

        //  On Scroll Listener
        mRecyclerView.addOnScrollListener(mEndlessRecyclerViewScrollListener);

        //swipe to refresh listener
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mEndlessRecyclerViewScrollListener.resetCurrentPage();// reset current after pagination
            mTotalCars.clear();
            checkingNetworkAvailablity();// check network and get data
        });

    }

    /*
    super functions
     */
    @Override
    public void WhenAllIsGood() {
        super.WhenAllIsGood();

        mSwipeRefreshLayout.setRefreshing(true);
        mPresenter.getCarsOnline();

    }


    /*
    views methods
     */
    @Override
    public void showError(Error error) {

    }

    @Override
    public void showError() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showCarsOnline(CarsResponse response) {
        isLoading = false;// enable loading more.
        mTotalCars.clear();

        if (isUpdating) {
            isUpdating = false;
            updateRecyclerView(response.getCars());
        } else {
            mTotalCars.addAll(response.getCars());
            showCarsList(0);

            if (isFirstTimeToRefresh) {
                isFirstTimeToRefresh = false;
                refreshCars(response.getRefreshInterval());

            }
        }

    }

    @Override
    public void setPresenter(CarsContract.Presenter presenter) {

    }


    String showCarsList(int page) {
        runOnUiThread(() -> {

            if (mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.setRefreshing(false);
                //mCars.clear();
            }

            // to hide loading layout
            if (mCars.size() > 0) {
                mCars.remove(mCars.size() - 1);
                mCarsAdapter.notifyItemRemoved(mCars.size());
            }


            start = Math.min(page * Constants.ITEMS_PER_PAGE, mTotalCars.size());
            end = Math.min((page * Constants.ITEMS_PER_PAGE) + Constants.ITEMS_PER_PAGE, mTotalCars.size());// minimum method to avoid out IndexOutOfBoundsException thr fake pagination;)
            Log.d("details", "page " + page + " start " + start + " end " + end + " size " + mTotalCars.size());

            if (mCars.size() == 0) {
                mCars = new ArrayList<>(mTotalCars.subList(start, end));
                mCarsAdapter = new CarsAdapter(MainActivity.this, mCars);
                mRecyclerView.setAdapter(mCarsAdapter);

            } else {

                mCars.addAll(new ArrayList<>(mTotalCars.subList(start, end)));
                mCarsAdapter.notifyDataSetChanged();

            }
        });
        isLoading = false;

        return "Done";
    }


    @OnClick({R.id.mOptions_iv})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.mOptions_iv:
                PopupMenu popupMenu = new PopupMenu(this, view);
                popupMenu.setOnMenuItemClickListener(item -> {

                    switch (item.getItemId()) {
                        case R.id.arabic:
                            if (Utils.isDeviceEnglish()) {
                                Utils.forceLocalization(this, "ar");
                                Intent intent = getIntent();
                                finish();
                                startActivity(intent);
                            }

                            return true;
                        case R.id.english:
                            if (Utils.isDeviceEnglish())
                                return true;

                            Utils.forceLocalization(this, "en_US");
                            Intent intent = getIntent();
                            finish();
                            startActivity(intent);
                            return true;
                    }

                    return false;
                });


                popupMenu.inflate(R.menu.menu);
                popupMenu.show();

                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSubscription != null)
            mSubscription.unsubscribe();

    }


}
