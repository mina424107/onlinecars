package com.emiratesauction.carsonline.views;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.emiratesauction.carsonline.R;
import com.emiratesauction.carsonline.adapter.CarsAdapter;
import com.emiratesauction.carsonline.app.App;
import com.emiratesauction.carsonline.data.models.Car;
import com.emiratesauction.carsonline.utils.Utils;
import com.koushikdutta.ion.Ion;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mina farid on 09/07/2018.
 */
public class CarsViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.mCar_iv)
    ImageView mCar_iv;

    @BindView(R.id.mCarModel_tv)
    TextView mCarModel_tv;

    @BindView(R.id.mCarBids_tv)
    TextView mCarBids_tv;

    @BindView(R.id.mCarLotNo_tv)
    TextView mCarLotNo_tv;

    @BindView(R.id.mCarTimerLeft_tv)
    TextView mCarTimerLeft_tv;

    @BindView(R.id.mCarPrice_tv)
    TextView mCarPrice_tv;

    @BindView(R.id.mCarPriceCurrency_tv)
    TextView mCarPriceCurrency_tv;


    CountDownTimer timer;

    public CarsViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);

    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    public void bind(final CarsAdapter adapter, int position) {
        // show data
        Car mCar = adapter.mCars.get(position);


        mCarModel_tv.setText(mCar.getCarName());
        mCarPrice_tv.setText(Utils.withSuffix(mCar.getAuctionInfo().getCurrentPrice()));
        mCarLotNo_tv.setText("" + mCar.getAuctionInfo().getLot());
        mCarBids_tv.setText("" + mCar.getAuctionInfo().getBids());
        mCarPriceCurrency_tv.setText(mCar.getAuctionInfo().getCurrency());


          /*
        to set image height to the half of screen width (dynamic)
         */
        try {
            mCar_iv.getLayoutParams().width = (int) (App.mScreenSize / 2);
        } catch (Exception ignored) {

        }


        Glide.with(adapter.mContext)
                .load(mCar.getImage().replace("[w]", String.valueOf(mCar_iv.getWidth())).replace("[h]", String.valueOf(mCar_iv.getHeight())))
                .apply(new RequestOptions()
                        .placeholder(new ColorDrawable(ContextCompat.getColor(adapter.mContext, R.color.colorGrey)))
                        .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontTransform())
                .into(mCar_iv);



        long millis = mCar.getAuctionInfo().getEndDate() * 1000;// convert secs to millisec

        long days = TimeUnit.MILLISECONDS.toDays(millis);
        millis -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

        if ((minutes < 5) & days == 0 && hours == 0) //Under 5 mints, color of time label will be change to red till it get updated
            mCarTimerLeft_tv.setTextColor(Color.RED);
        else
            mCarTimerLeft_tv.setTextColor(Color.BLACK);

        mCarTimerLeft_tv.setText(String.format("%02d:%02d:%02d:%02d", days, hours, minutes, seconds));

    }


}
