package com.emiratesauction.carsonline.views;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mina farid on 09/07/2018.
 */
public class LoadingViewHolder extends RecyclerView.ViewHolder {

    public LoadingViewHolder(View view) {
        super(view);

    }

    public void bind() {
    }
}