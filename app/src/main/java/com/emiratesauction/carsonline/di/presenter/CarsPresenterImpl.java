package com.emiratesauction.carsonline.di.presenter;

import com.emiratesauction.carsonline.data.source.DataRepository;
import com.emiratesauction.carsonline.data.source.remote.models.CarsResponse;
import com.emiratesauction.carsonline.di.presenter.BasePresenter;
import com.emiratesauction.carsonline.di.presenter.CarsContract;

import javax.inject.Inject;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mina farid on 09/07/2018.
 */
public class CarsPresenterImpl extends BasePresenter implements CarsContract.Presenter {

    private final DataRepository mDataRepository;
    private final CarsContract.View mView;

    /**
     * Dagger strictly enforces that arguments not marked with {@code @Nullable} are not injected
     * with {@code @Nullable} values.
     */
    @Inject
    CarsPresenterImpl(DataRepository dataRepository, CarsContract.View view) {
        mDataRepository = dataRepository;
        mView = view;
    }

    /**
     * Method injection is used here to safely reference {@code this} after the object is created.
     * For more information, see Java Concurrency in Practice.
     */
    @Inject
    void setupListeners() {
        mView.setPresenter(this);
    }


    @Override
    public void getCarsOnline() {
        mView.showLoading();
        mDataRepository.getCarsOnline().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CarsResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showError();
                    }

                    @Override
                    public void onNext(CarsResponse response) {
                        mView.hideLoading();
                        mView.showCarsOnline(response);
                    }
                });
    }


    @Override
    public void start() {

    }
}
