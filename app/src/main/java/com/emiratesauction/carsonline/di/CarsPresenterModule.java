package com.emiratesauction.carsonline.di;

import com.emiratesauction.carsonline.di.presenter.CarsContract;
import com.emiratesauction.carsonline.di.presenter.CarsPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mina farid on 09/07/2018.
 */
@Module
public class CarsPresenterModule {
    private final CarsContract.View mView;

    public CarsPresenterModule(CarsContract.View view) {
        this.mView = view;
    }

    @Provides
    CarsContract.View provideCarsView() {
        return mView;
    }

    @Provides
        // <-- provide constructor injected stuff and bind to interface
    CarsContract.Presenter registerCars(CarsPresenterImpl impl) {
        return impl;
    }
}
