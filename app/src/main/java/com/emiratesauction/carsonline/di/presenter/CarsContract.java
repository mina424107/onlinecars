package com.emiratesauction.carsonline.di.presenter;

import com.emiratesauction.carsonline.di.presenter.IBasePresenter;
import com.emiratesauction.carsonline.di.presenter.IBaseView;
import com.emiratesauction.carsonline.data.source.remote.models.CarsResponse;

/**
 * Created by mina farid on 09/07/2018.
 */
public interface CarsContract {
    interface View extends IBaseView<Presenter> {
        void showError(Error error);
        void showError();

        void showLoading();

        void hideLoading();

        void showCarsOnline(CarsResponse response);

    }

    interface Presenter extends IBasePresenter {

        void getCarsOnline();

    }
}
