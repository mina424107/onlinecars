package com.emiratesauction.carsonline.di;

import com.emiratesauction.carsonline.data.source.DataRepositoryComponent;
import com.emiratesauction.carsonline.ui.MainActivity;

import dagger.Component;

/**
 * Created by mina farid on 09/07/2018.
 */
@FragmentScoped
@Component(dependencies = DataRepositoryComponent.class, modules = CarsPresenterModule.class)
public interface CarsComponent {
    void inject(MainActivity activity);

}
