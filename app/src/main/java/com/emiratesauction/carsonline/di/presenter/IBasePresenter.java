package com.emiratesauction.carsonline.di.presenter;

/**
 * Created by mina farid on 08/07/2018.
 */

public interface IBasePresenter {

    void start();

}