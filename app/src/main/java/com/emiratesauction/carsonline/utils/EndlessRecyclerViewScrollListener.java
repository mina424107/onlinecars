package com.emiratesauction.carsonline.utils;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by mina farid on 09/07/2018.
 */

public abstract class EndlessRecyclerViewScrollListener extends RecyclerView.OnScrollListener {

    private int currentPage = 0;
    private LinearLayoutManager layoutManager;

    protected EndlessRecyclerViewScrollListener(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    public void resetCurrentPage() {
        currentPage = 0;
    }

    @Override
    public void onScrolled(RecyclerView view, int dx, int dy) {
        if (dy > 0) //check for scroll down
        {
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();


            if (!isLoading())
                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                    currentPage++;
                    onLoadMore(currentPage, totalItemCount);

                }
        }
    }

    public abstract void onLoadMore(int page, int totalItemsCount);

    public abstract boolean isLoading();

}