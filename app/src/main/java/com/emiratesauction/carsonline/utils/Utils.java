package com.emiratesauction.carsonline.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.ActionMenuView;
import android.widget.Toast;

import com.emiratesauction.carsonline.R;
import com.emiratesauction.carsonline.app.Constants;

import java.util.Locale;

/**
 * Created by mina fared on 9/25/2017.
 */

public class Utils {
    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {

            NetworkInfo[] networkInfos = connectivityManager.getAllNetworkInfo();
            for (NetworkInfo ni : networkInfos) {

                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        return true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        return true;
            }
        }
        return false;
    }

    public static void showToastError(Context context) {
        if (context == null)
            return;
        Toast toast = Toast.makeText(context, R.string.Some_thing_went_wrong, Toast.LENGTH_SHORT);
        toast.show();
    }

    public static void showToast(Context context, String text) {
        if (context == null)
            return;
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.show();
    }

    public static boolean isDeviceEnglish() {
        String language = Locale.getDefault().getDisplayLanguage();
        if (language.contains("ar") || language.contains("العربية"))
            return false;
        else
            return true;

    }


    public static void forceLocalization(Context context, String languageToLoad) {
        // force app language to english

        SharedPreferences prefs = context.getSharedPreferences(Constants.Prefs, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString("UserLanguage", languageToLoad);
        editor.apply();


        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            config.setLocale(locale);
        else
            config.locale = locale;

        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());


    }
    public static float getScreenWidth(Context activity) {


        DisplayMetrics displayMetrics = activity.getResources().getDisplayMetrics();
        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        dpWidth = dpWidth - 30;
        dpWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpWidth, displayMetrics);

        return dpWidth;
    }

    @SuppressLint("DefaultLocale")
    public static String withSuffix(long count) {
        if (count < 1000) return "" + count;
        int exp = (int) (Math.log(count) / Math.log(1000));
        return String.format("%.1f %c",
                count / Math.pow(1000, exp),
                "kMGTPE".charAt(exp - 1));
    }

}
