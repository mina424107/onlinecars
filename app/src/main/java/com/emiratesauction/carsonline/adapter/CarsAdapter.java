package com.emiratesauction.carsonline.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emiratesauction.carsonline.R;
import com.emiratesauction.carsonline.data.models.Car;
import com.emiratesauction.carsonline.views.CarsViewHolder;
import com.emiratesauction.carsonline.views.LoadingViewHolder;

import java.util.ArrayList;

/**
 * Created by mina farid on 09/07/2018.
 */
public class CarsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_LOADING = 1001;
    private final int VIEW_TYPE_CAR = 1002;
    public Context mContext;
    public ArrayList<Car> mCars;

    public CarsAdapter(Context mContext, ArrayList<Car> mCars) {
        this.mContext = mContext;
        this.mCars = mCars;
    }

    public int getItemCount() {
        return mCars.size();
    }

    public int getItemViewType(int position) {
        return (mCars.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_CAR);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View v = null;
        switch (viewType) {

            case VIEW_TYPE_CAR:
                v = inflater.inflate(R.layout.item_car, parent, false);
                viewHolder = new CarsViewHolder(v);
                break;
            case VIEW_TYPE_LOADING:
                v = inflater.inflate(R.layout.item_loading, parent, false);
                viewHolder = new LoadingViewHolder(v);
                break;

            default:
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int itemType = getItemViewType(position);
        switch (itemType) {
            case VIEW_TYPE_CAR:
                ((CarsViewHolder) holder).bind(this, position);
                break;
            case VIEW_TYPE_LOADING:
                ((LoadingViewHolder) holder).bind();
                break;
            default:
                break;
        }
    }


}