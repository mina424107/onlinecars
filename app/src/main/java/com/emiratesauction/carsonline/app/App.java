package com.emiratesauction.carsonline.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.emiratesauction.carsonline.R;
import com.emiratesauction.carsonline.data.source.AppModule;
import com.emiratesauction.carsonline.data.source.DaggerDataRepositoryComponent;
import com.emiratesauction.carsonline.data.source.DataRepositoryComponent;
import com.emiratesauction.carsonline.data.source.DataRepositoryModule;
import com.emiratesauction.carsonline.utils.Utils;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class App extends Application {

    private DataRepositoryComponent mDataRepositoryComponent;
    public static float mScreenSize;

    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/LatoRegular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        mDataRepositoryComponent = DaggerDataRepositoryComponent.builder()
                .appModule(new AppModule(getApplicationContext()))
                .dataRepositoryModule(new DataRepositoryModule())
                .build();

        loadUserLanaguage();
        mScreenSize=  Utils.getScreenWidth(getApplicationContext());
    }

    private void loadUserLanaguage() {
        SharedPreferences prefs = getSharedPreferences(Constants.Prefs, MODE_PRIVATE);
        String UserLanguage = prefs.getString("UserLanguage", null);
        if (UserLanguage != null) {
            if (UserLanguage.equals("ar"))
                Utils.forceLocalization(getBaseContext(), "ar");
            else
                Utils.forceLocalization(getBaseContext(), "en_US");

        }
    }


    public DataRepositoryComponent getNetComponent() {
        return mDataRepositoryComponent;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }


}
